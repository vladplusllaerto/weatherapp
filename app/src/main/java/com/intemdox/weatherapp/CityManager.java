package com.intemdox.weatherapp;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.intemdox.weatherapp.models.City;
import com.intemdox.weatherapp.utility.Constants;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class CityManager {
    public static final String TAG = "CityManager";

    private static List<City> myCityList;
    private static List<City> topCityList;
    private static CityManager instance;
    private Context context;

    private CityManager(Context context) {
        this.context = context;
    }

    public static void initialize(Context context) {
        instance = new CityManager(context);
    }

    public static CityManager getInstance() {
        return instance;
    }

    public List<City> getMyCityList() {
        if (myCityList == null) {
            initializeMyCityList();
        }
        return myCityList;
    }

    public List<City> getTopCityList() {
        if (topCityList == null) {
            initializeTopCityList();
        }
        return topCityList;
    }

    public void initializeTopCityList() {
        topCityList = new ArrayList<>();

        String[] myResArray = context.getResources().getStringArray(R.array.top_cities_array);
        List<String> myResArrayList = Arrays.asList(myResArray);
        for (String str : myResArrayList) {
            City city = new City();
            city.setCityName(str);
            topCityList.add(city);
        }
    }

    public void initializeMyCityList() {
        String json = "";
        StringBuilder sb = new StringBuilder();
        File path = Environment.getExternalStorageDirectory();
        File file = new File(path, Constants.MY_CITY_LIST_FILE_NAME);
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append('\n');
            }
            br.close();
            json = sb.toString();
        } catch (IOException e) {
            Log.e(TAG, "getMyCityList: IOException", e);
        }
        myCityList = new Gson().fromJson(json, new TypeToken<List<City>>() {
        }.getType());
        if (myCityList == null) {
            myCityList = new ArrayList<>();
        }
    }

    public void addCityToMyCityList(City city) {
        if (!myCityList.contains(city)) {
            myCityList.add(city);
            writeInMyCityList();
        } else {
            Log.d(TAG, "addCityToMyCityList: myCityList already contains this city");
        }
    }

    public void deleteCityFromMyCityList(City city) {
        if (myCityList.contains(city)) {
            myCityList.remove(city);
            writeInMyCityList();
        } else {
            Log.d(TAG, "deleteCityFromMyCityList: myCityList not contains this city");
        }
    }

    private void writeInMyCityList() {
        File path = Environment.getExternalStorageDirectory();
        File file = new File(path, Constants.MY_CITY_LIST_FILE_NAME);

        String json = new Gson().toJson(myCityList, new TypeToken<List<City>>() {
        }.getType());
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(file));
            bw.write(json);
            bw.close();
        } catch (IOException e) {
            Log.e(TAG, "writeInMyCityList: IOException", e);
        }
//        AppFacade.getInstance().getBus().post(new UpdateItemViewEvent());
    }



    public City getCityFromMyCityList(int position){
        try{
            return myCityList.get(position);
        }
        catch (Exception e){
            Log.d(TAG, "getCityFromMyCityList: not found city");
        }
        return new City();
    }
}
