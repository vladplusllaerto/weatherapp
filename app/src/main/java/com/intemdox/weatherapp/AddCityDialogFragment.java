package com.intemdox.weatherapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.intemdox.weatherapp.adapters.PlacesAutoCompleteAdapter;
import com.intemdox.weatherapp.events.DialogCityNameEvent;
import com.intemdox.weatherapp.events.InsertItemViewEvent;
import com.intemdox.weatherapp.listeners.RecyclerItemClickListener;
import com.intemdox.weatherapp.utility.Constants;


public class AddCityDialogFragment extends DialogFragment
        implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    // public static final String TAG = "AddCityDF";

    protected GoogleApiClient googleApiClient;

    private AutoCompleteTextView autocompleteView;
    private RecyclerView recyclerView;
    private ImageView delete;
    private LinearLayoutManager linearLayoutManager;
    private PlacesAutoCompleteAdapter autoCompleteAdapter;
    private static final LatLngBounds BOUNDS_INDIA = new LatLngBounds(
            new LatLng(-0, 0), new LatLng(0, 0));

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        buildGoogleApiClient();

        final LayoutInflater inflater = getActivity().getLayoutInflater();
        View root = inflater.inflate(R.layout.dialog_add_city, null, false);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(root).setPositiveButton(getResources().getString(R.string.save),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String result = autocompleteView.getText().toString();
                        AppFacade.getInstance().getBus().post(new DialogCityNameEvent(result));
                        Log.d("PrefActivity", "onClick: event was send");
                        AppFacade.getInstance().getBus().post(new InsertItemViewEvent(0));
                        dismiss();
                    }
                }).setNegativeButton(getResources().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        AddCityDialogFragment.this.getDialog().cancel();
                    }
                });

        delete = (ImageView) root.findViewById(R.id.cross);
        autocompleteView = (AutoCompleteTextView) root.findViewById(R.id.autocomplete_places);
        autoCompleteAdapter = new PlacesAutoCompleteAdapter(getActivity(), R.layout.search_row,
                googleApiClient, BOUNDS_INDIA, null);
        recyclerView = (RecyclerView) root.findViewById(R.id.recyclerView);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(autoCompleteAdapter);
        delete.setOnClickListener(this);

        autocompleteView.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (!s.toString().equals("") && googleApiClient.isConnected()) {
                    autoCompleteAdapter.getFilter().filter(s.toString());
                } else if (!googleApiClient.isConnected()) {
                    Toast.makeText(getActivity(), Constants.API_NOT_CONNECTED,
                            Toast.LENGTH_SHORT).show();
                    Log.e(Constants.PLACES_TAG, Constants.API_NOT_CONNECTED);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {

            }
        });

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(),
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                final PlacesAutoCompleteAdapter.PlaceAutocomplete item =
                                        autoCompleteAdapter.getItem(position);
                                final String placeId = String.valueOf(item.placeId);
                                Log.i("TAG", "Autocomplete item selected: " + item.description);
                        /*
                             Issue a request to the Places Geo Data API to retrieve
                             a Place object with additional details about the place.
                         */

                                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                                        .getPlaceById(googleApiClient, placeId);
                                placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                                    @Override
                                    public void onResult(@NonNull PlaceBuffer places) {
                                        if (places.getCount() == 1) {
                                            //Do the things here on Click.....
                                            Toast.makeText(getActivity(),
                                                    String.valueOf(places.get(0).getLatLng()),
                                                    Toast.LENGTH_SHORT).show();

                                            //add in autocomplete string
                                            autocompleteView.setText(String.valueOf(places.get(0)
                                                    .getAddress()));

                                        } else {
                                            Toast.makeText(getActivity(),
                                                    Constants.SOMETHING_WENT_WRONG,
                                                    Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                                Log.i("TAG", "Clicked: " + item.description);
                                Log.i("TAG", "Called getPlaceById to get Place details for "
                                        + item.placeId);
                            }
                        })
        );
        return builder.create();
    }

    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.v("Google API Callback", "Connection Done");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.v("Google API Callback", "Connection Suspended");
        Log.v("Code", String.valueOf(i));
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.v("Google API Callback", "Connection Failed");
        Log.v("Error Code", String.valueOf(connectionResult.getErrorCode()));
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (!googleApiClient.isConnected() && !googleApiClient.isConnecting()) {
            Log.v("Google API", "Connecting");
            googleApiClient.connect();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (googleApiClient.isConnected()) {
            Log.v("Google API", "Dis-Connecting");
            googleApiClient.disconnect();
        }
    }
}
