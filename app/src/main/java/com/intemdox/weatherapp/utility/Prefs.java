package com.intemdox.weatherapp.utility;

import android.content.Context;
import android.content.SharedPreferences;


public class Prefs {
    public static SharedPreferences sharedPreferences;

    public static void saveString(Context context, String key, String value) {
        sharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String loadString(Context context, String key) {
        sharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME,
                Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, "unknown");
    }
}
