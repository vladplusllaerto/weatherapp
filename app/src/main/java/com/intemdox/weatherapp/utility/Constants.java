package com.intemdox.weatherapp.utility;


public class Constants {
    public static final String API_NOT_CONNECTED = "Google API not connected";
    public static final String SOMETHING_WENT_WRONG = "OOPs!!! Something went wrong...";
    public static final String PLACES_TAG = "Google Places AutoCompl";
    public static final String PREFS_NAME = "weatherAppsPrefs";
    public static final String LOCATION_NAME = "location_name";
    public static final String EMPTY_RESULT = "";
    public static final String MY_CITY_LIST_FILE_NAME = "cityList.itx";
    public static final String C = "\u00B0";
    public static final String F = "\u00B0";
}