package com.intemdox.weatherapp;


import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import android.location.Address;
import android.widget.Toast;

import com.google.android.gms.location.LocationServices;
import com.intemdox.weatherapp.utility.Constants;
import com.intemdox.weatherapp.utility.Prefs;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class LocationHelper implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    public static final String TAG = "LocationHelper";

    private static LocationHelper instance;
    private GoogleApiClient googleApiClient;
    private LocationManager locationManager;
    private Context context;
    private String address;

    private LocationHelper(Context context) {
        this.context = context;
        googleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this).addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    public static void initialize(Context context) {
        if (instance == null) {
            instance = new LocationHelper(context);
        }
    }

    public void connect() {
        googleApiClient.connect();
    }

    public void disconnect() {
        googleApiClient.disconnect();
    }

    public static LocationHelper getInstance() {
        return instance;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.v("Google API Callback", "Connection Done");
    }

    public String getLocationName() {
        LocationListener locationListener = new MyLocationListener();
        if (checkInternetPermission()) {
            Location lastKnownLocation = locationManager
                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (lastKnownLocation == null) {
                Log.d(TAG, "useLastKnownLocation: GPS NOT WORK");
                lastKnownLocation = locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            } else {
                Log.d(TAG, "useLastKnownLocation: location != null" + "lat = "
                        + lastKnownLocation.getLatitude() + " long = "
                        + lastKnownLocation.getLongitude());
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        5000L, 10f, locationListener);
            }
            if (lastKnownLocation != null) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0,
                        locationListener);
            } else {
                Log.d(TAG, "useLastKnownLocation: location == null");
            }
        }
        Prefs.saveString(context, Constants.LOCATION_NAME, address);
        return address;
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.v("Google API Callback", "Connection Suspended");
        Log.v("Code", String.valueOf(i));
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.v("Google API Callback", "Connection Failed");
        Log.v("Error Code", String.valueOf(connectionResult.getErrorCode()));
        Toast.makeText(context, Constants.API_NOT_CONNECTED, Toast.LENGTH_SHORT).show();
    }

    //Listener class to get coordinates
    class MyLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            Log.d(TAG, "onLocationChanged: Location changed: Lat = " + location.getLatitude()
                    + ": Long = " + location.getLongitude());
            Geocoder gcd = new Geocoder(context, Locale.getDefault());
            List<Address> addresses;
            try {
                Log.d(TAG, "onLocationChanged: get address...");
                addresses = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                if (addresses.size() > 0) {
                    Log.d(TAG, "onLocationChanged: address.size > 0...");
                    Log.d(TAG, "onLocationChanged: " + addresses.get(0).getAddressLine(0));
                    Log.d(TAG, "onLocationChanged: " + addresses.get(0).getAddressLine(1));
                    Log.d(TAG, "onLocationChanged: " + addresses.get(0).getAddressLine(2));
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < addresses.size(); i++) {
                        sb.append(addresses.get(0).getAddressLine(i));
                    }
                    address = sb.toString();

                    Log.d(TAG, "onLocationChanged: address = " + address);

                } else {
                    Log.d(TAG, "onLocationChanged: addresses is empty");
                }
            } catch (IOException e) {
                Log.e(TAG, "onLocationChanged: IOException", e);
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    }

    private boolean checkInternetPermission() {
        String permission = "android.permission.INTERNET";
        int res = context.checkCallingOrSelfPermission(permission);
        return res == PackageManager.PERMISSION_GRANTED;
    }
}
