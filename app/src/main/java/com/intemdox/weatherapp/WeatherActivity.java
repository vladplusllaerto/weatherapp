package com.intemdox.weatherapp;


import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.intemdox.weatherapp.adapters.MiniWeatherAdapter;
import com.intemdox.weatherapp.common.AppError;
import com.intemdox.weatherapp.common.Callback;
import com.intemdox.weatherapp.events.ApplySettingsEvent;
import com.intemdox.weatherapp.models.ModelWeather;
import com.intemdox.weatherapp.utility.Constants;
import com.intemdox.weatherapp.utility.Prefs;
import com.squareup.otto.Subscribe;

public class WeatherActivity extends MainActivity {
    private TextView tvTemperature;
    private TextView tvLocation;
    private TextView tvWindSpeed;
    private TextView tvHumidity;
    private TextView tvWeatherDescription;
    private TextView tvVisibility;
    private TextView tvSunrise;
    private TextView tvSunset;
    private ImageView ivImageDescription;
    private ProgressDialog progressDialog;
    private RecyclerView recyclerViewMiniWeather;

    public static final String TAG = "WeatherActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        initDrawer();
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            AppFacade.getInstance().getBus().register(this);
        } catch (Exception e) {
            Log.d(TAG, "onResume: Object already registered.");
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppFacade.getInstance().getBus().unregister(this);
    }

    public void init() {
        tvTemperature = (TextView) findViewById(R.id.tv_weather_activity_temperature);
        tvLocation = (TextView) findViewById(R.id.tv_weather_activity_location_name);
        tvWindSpeed = (TextView) findViewById(R.id.tv_weather_activity_speed_of_wind);
        tvHumidity = (TextView) findViewById(R.id.tv_weather_activity_humidity);
        tvWeatherDescription = (TextView) findViewById(R.id.tv_weather_activity_description);
        tvVisibility = (TextView) findViewById(R.id.tv_weather_activity_visibility);
        tvSunrise = (TextView) findViewById(R.id.tv_weather_activity_sunrise);
        tvSunset = (TextView) findViewById(R.id.tv_weather_activity_sunset);
        ivImageDescription = (ImageView) findViewById(R.id.iv_weather_activity_weather_icon);
        recyclerViewMiniWeather = (RecyclerView) findViewById(R.id.recycler_weather_activity_mini);
        recyclerViewMiniWeather.setLayoutManager(new LinearLayoutManager(WeatherActivity.this,
                LinearLayoutManager.HORIZONTAL, false));

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        String loc;
            try {
                loc = getIntent().getExtras().getString(Constants.LOCATION_NAME);
            } catch (NullPointerException e) {
                loc = AppFacade.getInstance().getLocationName();
            }
        if (loc != null) {
            if (loc.equals("") || loc.equals(getResources().getString(R.string.unknown_city))) {
                loc = Prefs.loadString(this, Constants.LOCATION_NAME);
            }
        }else{
            loc = "Tokyo, Japan";
        }
        RESTHelper.initialize();
        RESTHelper.getInstance().refreshWeather(loc, new Callback<ModelWeather, AppError>() {
            @Override
            public void onCompleted(ModelWeather result) {
                tvTemperature.setText(result.getTemperatureC());
                tvSunset.setText(result.getSunset());
                tvSunrise.setText(result.getSunrise());
                tvLocation.setText(result.getLocationName());
                tvWindSpeed.setText(result.getSpeedOfWind());
                tvHumidity.setText(result.getHumidity());
                tvWeatherDescription.setText(result.getWeatherDescription());
                tvVisibility.setText(result.getVisibility());
                int resID = getResources().getIdentifier("drawable/weather_icon_" + result.getCode(), null, getPackageName());
                @SuppressWarnings("deprecation")
                Drawable weatherIconDrawable = getResources().getDrawable(resID);
                ivImageDescription.setImageDrawable(weatherIconDrawable);
                recyclerViewMiniWeather.setAdapter(new MiniWeatherAdapter(WeatherActivity.this, result));
                progressDialog.hide();
            }

            @Override
            public void onError(AppError error) {
                Log.d(TAG, "onError: nothing");
                progressDialog.hide();
            }
        });
    }

    @Subscribe
    public void on(ApplySettingsEvent event) {
        recreate();
    }
}