package com.intemdox.weatherapp;


import android.content.Context;
import android.util.Log;

import com.intemdox.weatherapp.utility.Constants;
import com.intemdox.weatherapp.utility.Prefs;
import com.squareup.otto.Bus;

public class AppFacade {
    private static final String TAG = "AppFacade";
    private Context context;
    private static AppFacade instance;
    private Bus bus;

    private AppFacade(Context context) {
        this.bus = new Bus();
        this.context = context;
    }

    public static void initialize(Context context) {
        if (instance == null) {
            instance = new AppFacade(context);
        }
    }

    public static AppFacade getInstance() {
        return instance;
    }

    public Bus getBus() {
        return bus;
    }

    public String getLocationName() {
        String locationName = Prefs.loadString(context, Constants.LOCATION_NAME);
        Log.d(TAG, "getLocationName: " + locationName);

        if (!locationName.equals(Constants.EMPTY_RESULT)
                && !locationName.equals(context.getResources().getString(R.string.unknown_city))
                && !locationName.isEmpty()) {
            Log.d(TAG, "getLocationName: locationName is not empty");
            return locationName;
        } else {
            Log.d(TAG, "getLocationName: locationName is empty");
            LocationHelper.initialize(context);
            LocationHelper locationHelper = LocationHelper.getInstance();
            locationHelper.connect();
            String newLocation = locationHelper.getLocationName();
            if (newLocation == null) {
                newLocation = context.getResources().getString(R.string.unknown_city);
            }
            locationHelper.disconnect();
            Prefs.saveString(context, Constants.LOCATION_NAME, newLocation);
            return newLocation;
        }
    }
}
