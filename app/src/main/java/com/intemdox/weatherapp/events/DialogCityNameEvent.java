package com.intemdox.weatherapp.events;


public class DialogCityNameEvent {
    private String cityName;

    public DialogCityNameEvent(String cityName){
        this.cityName = cityName;
    }

    public String getCityName() {
        return cityName;
    }
}
