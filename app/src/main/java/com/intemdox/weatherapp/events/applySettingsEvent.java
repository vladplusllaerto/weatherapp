package com.intemdox.weatherapp.events;


public class ApplySettingsEvent {
    private String cityName;

    public ApplySettingsEvent(String cityName){
        this.cityName = cityName;
    }

    public String getCityName() {
        return cityName;
    }
}
