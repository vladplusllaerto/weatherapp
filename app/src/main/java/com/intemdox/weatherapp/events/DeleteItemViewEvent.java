package com.intemdox.weatherapp.events;


public class DeleteItemViewEvent {
    int position;

    public DeleteItemViewEvent(int position){
        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}
