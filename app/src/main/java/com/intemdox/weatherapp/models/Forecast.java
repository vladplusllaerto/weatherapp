package com.intemdox.weatherapp.models;


import com.intemdox.weatherapp.utility.Constants;

public class Forecast {
    private double code;
    private double high;
    private double low;
    private String description;
    private String day;
    private String date;

    public Forecast(double code, double high, double low, String description, String day, String date) {
        this.code = code;
        this.high = high;
        this.low = low;
        this.description = description;
        this.day = day;
        this.date = date;
    }

    public int getCode() {
        return (int) code;
    }

    public String getHighC() {
        return  (int)((high - 32) * 5/9) + Constants.C;
    }

    public String getLowC() {
        return  (int)((low - 32) * 5/9) + Constants.C;
    }

    public String getDescription() {
        return description;
    }

    public String getDay() {
        return day;
    }

    public String getDate() {
        return date;
    }
}