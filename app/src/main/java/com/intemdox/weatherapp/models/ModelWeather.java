package com.intemdox.weatherapp.models;


import com.intemdox.weatherapp.utility.Constants;

public class ModelWeather {
    private String city;
    private String country;
    private String region;
    private String sunrise;
    private String sunset;
    private String weatherDescription;

    private double temperature;
    private double speedOfWind;
    private double directionOfWind;
    private double chillOfWind;
    private double humidity;
    private double pressure;
    private double visibility;
    private int code;
    private Forecast[] forecastArray;


    public ModelWeather() {

    }

    public Forecast[] getForecastArray() {
        return forecastArray;
    }

    public void setForecastArray(Forecast[] forecastArray) {
        this.forecastArray = forecastArray;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getSunrise() {
        return "sunrise: " + sunrise;
    }

    public String getLocationName() {
        return city + ", " + country;
    }

    public void setSunrise(String sunrise) {
        this.sunrise = sunrise;
    }

    public String getSunset() {
        return "sunset: " + sunset;
    }

    public void setSunset(String sunset) {
        this.sunset = sunset;
    }

    public String getWeatherDescription() {
        return weatherDescription;
    }

    public void setWeatherDescription(String weatherDescription) {
        this.weatherDescription = weatherDescription;
    }

    public String getTemperatureC() {
        int a = (int) ((temperature - 32) * 5 / 9);
        if (a > 0) {
            return "+" + a + Constants.C;
        } else {
            return a + Constants.C;
        }
    }

    public double getTemperatureF() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public String getSpeedOfWind() {
        return "wind: " + Math.round(speedOfWind * 1.609344) * 1000 / 3600 + "m/sec";
    }

    public void setSpeedOfWind(double speedOfWind) {
        this.speedOfWind = speedOfWind;
    }

    public double getDirectionOfWind() {
        return directionOfWind;
    }

    public void setDirectionOfWind(double directionOfWind) {
        this.directionOfWind = directionOfWind;
    }

    public double getChillOfWind() {
        return chillOfWind;
    }

    public void setChillOfWind(double chillOfWind) {
        this.chillOfWind = chillOfWind;
    }

    public String getHumidity() {
        return "humidity: " + humidity + "%";
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public double getPressure() {
        return pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public String getVisibility() {
        return "visibility: " + visibility + "km";
    }

    public void setVisibility(double visibility) {
        this.visibility = visibility;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }


}
