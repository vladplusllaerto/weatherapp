package com.intemdox.weatherapp.models;


public class City {

    private String cityName;

    public City() {

    }

    public String getCityName() {
        if (cityName == null) {
            cityName = "unknown";
        }
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
