package com.intemdox.weatherapp;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.intemdox.weatherapp.adapters.CityAdapter;
import com.intemdox.weatherapp.events.ApplySettingsEvent;
import com.intemdox.weatherapp.events.DeleteItemViewEvent;
import com.intemdox.weatherapp.events.DialogCityNameEvent;
import com.intemdox.weatherapp.events.InsertItemViewEvent;
import com.intemdox.weatherapp.models.City;
import com.squareup.otto.Subscribe;

public class CityListActivity extends MainActivity {

    public static final String INTENT_CITY_LIST = "INTENT_TOP_CITY_LIST";
    public static final String TAG = "CityListActivity";
    public static final int TOP_LIST = 0;
    public static final int MY_LIST = 1;

    private AddCityDialogFragment addCityDialogFragment;
    private RecyclerView cityList;
    private CityAdapter cityAdapter;
    private int type = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_list);
        initDrawer();
        type = getIntent().getExtras().getInt(INTENT_CITY_LIST);
        CityManager.initialize(this);
        addCityDialogFragment = new AddCityDialogFragment();
        Button addCity = (Button) findViewById(R.id.btnAddCity);
        if (addCity != null) {
            addCity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addCityDialogFragment.show(getFragmentManager(), "Dialog");
                }
            });
        }
        cityList = (RecyclerView) findViewById(R.id.recycler_city);
        if (cityList != null) {
            Log.d(TAG, "onCreate: cityList != null");
            cityList.setLayoutManager(new LinearLayoutManager(CityListActivity.this));
        } else {
            Log.e(TAG, "onCreate: cityList == null", null);
        }
        initializeList();
    }

    private void initializeList() {
        switch (type) {
            case MY_LIST:
                initializeMyList();
                break;
            case TOP_LIST:
                initializeTopList();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppFacade.getInstance().getBus().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppFacade.getInstance().getBus().unregister(this);
    }

    private void initializeMyList() {
        CityManager.getInstance().initializeMyCityList();
        setupAdapter();
    }

    private void initializeTopList() {
        CityManager.getInstance().initializeTopCityList();
        setupAdapter();
    }

    private void setupAdapter() {
        if (type == MY_LIST) {
            cityAdapter = new CityAdapter(this, CityManager.getInstance().getMyCityList());
        } else {
            cityAdapter = new CityAdapter(this, CityManager.getInstance().getTopCityList());
        }
        cityList.setAdapter(cityAdapter);
    }

    @Subscribe
    public void on(ApplySettingsEvent event) {
        tvHeaderCity.setText(event.getCityName());
    }

    @Subscribe
    public void on(InsertItemViewEvent event) {
        event.getClass();
        cityAdapter.notifyItemInserted(event.getPosition());
    }
    @Subscribe
    public void on(DeleteItemViewEvent event) {
        event.getClass();
        CityManager.getInstance().deleteCityFromMyCityList(CityManager.getInstance()
                .getCityFromMyCityList(event.getPosition()));
    }

    @Subscribe
    public void on(DialogCityNameEvent event) {
        //TODO right implementation
        City city = new City();
        city.setCityName(event.getCityName());
        CityManager.getInstance().addCityToMyCityList(city);
    }
}
