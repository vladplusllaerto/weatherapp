package com.intemdox.weatherapp.common;

/**
 * Created by Mykhailo Diachenko
 * on 15.02.2016.
 */
public class AppError extends Exception {
    private Throwable cause;

    public AppError(Throwable cause) {
        super(cause);
        this.cause = cause;
    }

    public AppError() {
        this(null);
    }

    @Override
    public Throwable getCause() {
        return cause;
    }
}
