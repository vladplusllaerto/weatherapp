package com.intemdox.weatherapp.common;

/**
 * Created by Mykhailo Diachenko
 * on 15.02.2016.
 */
public abstract class Chain<T, N> {
    public interface Done {
        void done(Throwable throwable);
    }

    public abstract T execute(N next, Done done);
}
