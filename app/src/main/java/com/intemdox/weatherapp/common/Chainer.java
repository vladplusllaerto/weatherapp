package com.intemdox.weatherapp.common;

import java.util.LinkedList;

/**
 * Created by Mykhailo Diachenko
 * on 15.02.2016.
 */
public class Chainer {

    LinkedList<Chain<?, ?>> chains = new LinkedList<>();
    private Chain.Done done;

    public <Runnable, N> Chainer when(Chain<Runnable, N> chain) {
        if (chains.size() > 0) {
            throw new IllegalStateException("when can be run just once");
        }
        chains.add(chain);
        return this;
    }

    public <T, N> Chainer then(Chain<T, N> chain) {
        if (chains.size() == 0) {
            throw new IllegalStateException("when should be run before");
        }
        chains.add(chain);
        return this;
    }

    public Chainer done(Chain.Done done) {
        if (chains.size() == 0) {
            throw new IllegalStateException("Nothing to run");
        }

        this.done = done;

        this.resolve();

        return this;
    }

    private void resolve() {
        Object next = this.done;
        do {
            @SuppressWarnings("unchecked")
            Chain<Object, Object> chain = (Chain<Object, Object>) chains.removeLast();
            next = chain.execute(next, this.done);
            if (chains.size() == 0) {
                Runnable starter = (Runnable)next;
                starter.run();
                break;
            }
        } while (true);
    }
}
