package com.intemdox.weatherapp.common;

/**
 * Created by Mykhailo Diachenko
 * on 15.02.2016.
 */
public interface Callback<T, E extends AppError> {
    void onCompleted(T result);
    void onError(E error);
}
