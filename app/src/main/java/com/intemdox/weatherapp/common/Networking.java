package com.intemdox.weatherapp.common;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.vedidev.restifizer.RestifizerManager;

/**
 * Created by Mykhailo Diachenko
 * on 15.02.2016.
 */
public class Networking {

    public static final String SERVER_URL = ""; //TODO - server URL here


    private static Networking instance;
    private static Context context;
//    private final RestifizerManager restifizerManager;
    private RequestQueue requestQueue;
    private ImageLoader imageLoader;

    private Networking(Context context) {
        Networking.context = context;
        requestQueue = getRequestQueue();
//        imageLoader = new ImageLoader(requestQueue, LruBitmapCache.getInstance(context));

//        restifizerManager = new RestifizerManager().
//                setRequestQueue(requestQueue).
//                setBaseUrl(SERVER_URL).
//                setErrorHandler(AppFacade.getInstance().getErrorHandler());
    }

    public static synchronized Networking getInstance(Context context) {
        if (instance == null) {
            instance = new Networking(context);
        }
        return instance;
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return requestQueue;
    }

//    uncomment it if necessary
//    public <T> void addToRequestQueue(Request<T> req) {
//        getRequestQueue().add(req);
//    }

    public ImageLoader getImageLoader() {
        return imageLoader;
    }

//    public RestifizerManager getRestifizerManager() {
//        return restifizerManager;
//    }


}
