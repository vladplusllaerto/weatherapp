package com.intemdox.weatherapp;


import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.intemdox.weatherapp.common.AppError;
import com.intemdox.weatherapp.common.Callback;
import com.intemdox.weatherapp.events.ApplySettingsEvent;
import com.intemdox.weatherapp.models.ModelWeather;
import com.intemdox.weatherapp.utility.Constants;
import com.intemdox.weatherapp.utility.Prefs;
import com.squareup.otto.Subscribe;


public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";

    public DrawerLayout drawerLayout;
    public Toolbar toolbar;
    public NavigationView navigationView;
    public ActionBarDrawerToggle actionBarDrawerToggle;
    public CharSequence drawerTitle;
    public CharSequence title;
    public View headerLayout;
    public TextView tvHeaderTemperature;
    public TextView tvHeaderCity;
    public ImageView headerWeatherIcon;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppFacade.initialize(this);
    }

    protected void selectItem(int id) {
        Intent intent;
        switch (id) {
            case R.id.navigation_item_my_city_activity:
                intent = new Intent(this, CityListActivity.class);
                intent.putExtra(CityListActivity.INTENT_CITY_LIST, 1);
                break;
            case R.id.navigation_item_weather_activity:
                intent = new Intent(this, WeatherActivity.class);
                break;
            case R.id.navigation_item_top_city_activity:
                intent = new Intent(this, CityListActivity.class);
                intent.putExtra(CityListActivity.INTENT_CITY_LIST, 0);
                break;
            case R.id.navigation_item_settings_activity:
                intent = new Intent(this, PrefActivity.class);
                break;
            default:
                intent = new Intent(this, WeatherActivity.class);
                break;
        }
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * This method initializes drawer.
     */
    protected void initDrawer() {
        title = drawerTitle = getTitle();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitleTextColor(ContextCompat
                    .getColor(getApplicationContext(), R.color.icons));
            setSupportActionBar(toolbar);
        }
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        toolbar.setTitleTextColor(ContextCompat.getColor(getApplicationContext(), R.color.icons));
        setSupportActionBar(toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        navigationView = (NavigationView) findViewById(R.id.nav_view);

        if (navigationView != null) {
            navigationView.setNavigationItemSelectedListener(
                    new NavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(MenuItem menuItem) {
                            menuItem.setChecked(true);
                            selectItem(menuItem.getItemId());
                            drawerLayout.closeDrawer(GravityCompat.START);
                            return true;
                        }
                    });
            headerLayout = navigationView.getHeaderView(0);
        } else {
            Log.e(TAG, "initDrawer: navigationView == null", null);
        }

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (getActionBar() != null) {
                    getActionBar().setTitle(drawerTitle);
                }
                Toast.makeText(MainActivity.this, getResources().getString(R.string.drawer_open),
                        Toast.LENGTH_SHORT).show();
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (getActionBar() != null) {
                    getActionBar().setTitle(title);
                }
                Toast.makeText(MainActivity.this, getResources().getString(R.string.drawer_close),
                        Toast.LENGTH_SHORT).show();
                invalidateOptionsMenu();
            }
        };
        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);  // OPEN DRAWER
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Subscribe
    public void on(ApplySettingsEvent event) {
        tvHeaderCity.setText(event.getCityName());
    }
}