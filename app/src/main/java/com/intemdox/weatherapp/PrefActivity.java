package com.intemdox.weatherapp;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.widget.Toast;

import com.intemdox.weatherapp.events.ApplySettingsEvent;
import com.intemdox.weatherapp.events.DialogCityNameEvent;
import com.intemdox.weatherapp.utility.Constants;
import com.intemdox.weatherapp.utility.Prefs;
import com.squareup.otto.Subscribe;


public class PrefActivity extends PreferenceActivity {

    public static final String TAG = "PrefActivity";

    private static AddCityDialogFragment dialog;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dialog = new AddCityDialogFragment();
        getFragmentManager().beginTransaction().replace(android.R.id.content,
                new MyPreferenceFragment()).commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        AppFacade.getInstance().getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        AppFacade.getInstance().getBus().unregister(this);
    }

    @Subscribe
    public void on(DialogCityNameEvent event) {
        Prefs.saveString(this, Constants.LOCATION_NAME, event.getCityName());
        Toast.makeText(this, event.getCityName(), Toast.LENGTH_SHORT).show();
        AppFacade.getInstance().getBus()
                .post(new ApplySettingsEvent(event.getCityName()));
    }

    public static class MyPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref);
            Preference changeLocation = findPreference("location_name");
            changeLocation.setDefaultValue(getResources().getString(R.string.unknown_city));
            changeLocation.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    dialog.show(getFragmentManager(), "PrefActivity");
                    return false;
                }
            });


//            CheckBoxPreference notification;
//            CheckBoxPreference degree;
        }
    }
}