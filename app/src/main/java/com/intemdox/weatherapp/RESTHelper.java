package com.intemdox.weatherapp;


import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.intemdox.weatherapp.common.AppError;
import com.intemdox.weatherapp.common.Callback;
import com.intemdox.weatherapp.models.Forecast;
import com.intemdox.weatherapp.models.ModelWeather;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class RESTHelper {

    private static RESTHelper instance;

    private RESTHelper() {

    }

//    public static void initialize(WeatherCallback callback) {
//        instance = new RESTHelper(callback);
//    }


    public static void initialize() {
        instance = new RESTHelper();
    }

    public static RESTHelper getInstance() {
        return instance;
    }

    public void refreshWeather(String location, final Callback<ModelWeather, AppError> callback) {

        String YQL = String.format("select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"%s\")", location);
        String stringURL = String.format("https://query.yahooapis.com/v1/public/yql?q=%s&format=json", Uri.encode(YQL));
//         String stringURL = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22nome%2C%20ak%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";


        new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String... params) {
                BufferedReader bufferedReader;
                HttpURLConnection httpURLConnection;
                try {
                    URL url = new URL(params[0]);
                    httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestProperty("Content-Type", "application/json");
                    httpURLConnection.setRequestMethod("GET");
                    httpURLConnection.connect();

                    InputStream inputStream = httpURLConnection.getInputStream();
                    bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        sb.append(line);
                    }
                    String finalJson = sb.toString();
                    bufferedReader.close();
                    httpURLConnection.disconnect();

                    return finalJson;
                } catch (IOException e) {
//                Log.e(TAG, "initialize: IOException", e);
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                try {
                    JSONObject data = new JSONObject(result);
                    JSONObject queryResults = data.optJSONObject("query");
                    int count = queryResults.optInt("count");
                    if (count == 0) {
                        callback.onError(new AppError());
                        //  weatherCallback.serviceFailure(new LocationWeatherException("No weather information found for this location"));
                    } else {
                        JSONObject results = queryResults.optJSONObject("results");
                        JSONObject channel = results.optJSONObject("channel");

                        JSONObject locationJSON = channel.optJSONObject("location");
                        String city = locationJSON.optString("city");
                        String country = locationJSON.optString("country");
                        String region = locationJSON.optString("region");

                        JSONObject windJSON = channel.optJSONObject("wind");
                        double speedOfWind = windJSON.optDouble("speed");
                        double directionOfWind = windJSON.optDouble("direction");
                        double chillOfWind = windJSON.optDouble("chill");   //холодность?

                        JSONObject atmosphereJSON = channel.optJSONObject("atmosphere");
                        double humidity = atmosphereJSON.optDouble("humidity"); //влажность
                        double pressure = atmosphereJSON.optDouble("pressure"); //давление
                        double visibility = atmosphereJSON.optDouble("visibility");

                        JSONObject astronomyJSON = channel.optJSONObject("astronomy");
                        String sunrise = astronomyJSON.optString("sunrise");
                        String sunset = astronomyJSON.optString("sunset");

                        JSONObject item = channel.optJSONObject("item");

                        JSONObject conditionJSON = item.optJSONObject("condition");
                        String code = conditionJSON.optString("code");
                        double temperature = conditionJSON.optDouble("temp");
                        String description = conditionJSON.optString("text");

                        JSONArray foracastJSONArray = item.optJSONArray("forecast");
                        Log.d("RRRA", "onPostExecute: forecast" + foracastJSONArray);

                        Forecast[] forecastArray = new Forecast[10];

                        for (int i = 0; i < foracastJSONArray.length(); i++) {
                            JSONObject forecastJSONObject = foracastJSONArray.optJSONObject(i);
                            double forecastCode = forecastJSONObject.optDouble("code");
                            String forecastDate = forecastJSONObject.optString("date");
                            String forecastDay = forecastJSONObject.optString("day");
                            double forecastHigh = forecastJSONObject.optDouble("high");
                            double forecastLow = forecastJSONObject.optDouble("low");
                            String forecastDescription = forecastJSONObject.optString("text");
                            forecastArray[i] = new Forecast(forecastCode, forecastHigh, forecastLow, forecastDescription, forecastDay, forecastDate);
                        }


                        ModelWeather model = new ModelWeather();
                        model.setCity(city);
                        model.setCountry(country);
                        model.setRegion(region);
                        model.setSpeedOfWind(speedOfWind);
                        model.setDirectionOfWind(directionOfWind);
                        model.setChillOfWind(chillOfWind);
                        model.setHumidity(humidity);
                        model.setPressure(pressure);
                        model.setVisibility(visibility);
                        model.setSunrise(sunrise);
                        model.setSunset(sunset);
                        model.setCode(Integer.parseInt(code));
                        model.setTemperature(temperature);
                        Log.d("RRRRA", "onPostExecute: temp = " + temperature);
                        model.setWeatherDescription(description);
                        model.setForecastArray(forecastArray);
                        callback.onCompleted(model);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(stringURL);
    }


}
