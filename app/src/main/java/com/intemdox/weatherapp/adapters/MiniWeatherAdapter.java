package com.intemdox.weatherapp.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.intemdox.weatherapp.R;
import com.intemdox.weatherapp.models.Forecast;
import com.intemdox.weatherapp.models.ModelWeather;

public class MiniWeatherAdapter extends RecyclerView.Adapter<MiniWeatherAdapter.MiniWeatherViewHolder> {

    Context context;
    ModelWeather model;
    Forecast[] forecastArray;

    public MiniWeatherAdapter(Context context, ModelWeather model) {
        this.context = context;
        this.model = model;
        forecastArray = model.getForecastArray();
    }

    @Override
    public MiniWeatherViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_mini_weather, parent, false);
        TextView tvDay = (TextView) v.findViewById(R.id.card_mini_day);
        TextView tvDate = (TextView) v.findViewById(R.id.card_mini_date);
        ImageView ivImage = (ImageView) v.findViewById(R.id.card_mini_img);
        TextView tvTemperature = (TextView) v.findViewById(R.id.card_mini_temperature);
        return new MiniWeatherViewHolder(v, tvDate, tvDay, tvTemperature, ivImage);
    }

    @Override
    public void onBindViewHolder(MiniWeatherViewHolder holder, int position) {
        if (position < forecastArray.length) {
            Forecast forecast = forecastArray[position];
            holder.day.setText(forecast.getDay());
            holder.date.setText(forecast.getDate());
            String temp = forecast.getHighC() + " / " + forecast.getLowC();
            holder.tvTemperature.setText(temp);
            int resID = context.getResources().getIdentifier("drawable/weather_icon_" + forecast.getCode(), null, context.getPackageName());
            @SuppressWarnings("deprecation")
            Drawable weatherIconDrawable = context.getResources().getDrawable(resID);
            holder.img.setImageDrawable(weatherIconDrawable);
        }
    }

    @Override
    public int getItemCount() {
        return forecastArray.length;
    }

    class MiniWeatherViewHolder extends RecyclerView.ViewHolder {
        TextView tvTemperature;
        TextView day;
        ImageView img;
        TextView date;

        public MiniWeatherViewHolder(View itemView, TextView date, TextView day, TextView temperature, ImageView img) {
            super(itemView);
            this.tvTemperature = temperature;
            this.day = day;
            this.img = img;
            this.date = date;
        }
    }
}
