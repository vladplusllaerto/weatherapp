package com.intemdox.weatherapp.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.intemdox.weatherapp.AppFacade;
import com.intemdox.weatherapp.R;
import com.intemdox.weatherapp.RESTHelper;
import com.intemdox.weatherapp.WeatherActivity;
import com.intemdox.weatherapp.common.AppError;
import com.intemdox.weatherapp.common.Callback;
import com.intemdox.weatherapp.events.DeleteItemViewEvent;
import com.intemdox.weatherapp.models.City;
import com.intemdox.weatherapp.models.ModelWeather;
import com.intemdox.weatherapp.utility.Constants;

import java.util.List;


public class CityAdapter extends RecyclerView.Adapter<CityAdapter.CityViewHolder> {
    public static final String TAG = "CityAdapter";
    private List<City> cityList;
    private Context context;

    public CityAdapter(Context context, List<City> list) {
        this.context = context;
        this.cityList = list;
    }

    @Override
    public CityAdapter.CityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_city_weather, parent, false);
        TextView tvCityName = (TextView) v.findViewById(R.id.cityName);
        TextView tvCountryName = (TextView) v.findViewById(R.id.countryName);
        TextView tvTemperature = (TextView) v.findViewById(R.id.temperature);
        TextView tvWindSpeed = (TextView) v.findViewById(R.id.windSpeed);
        ImageView imageWeather = (ImageView) v.findViewById(R.id.imageWeather);
        return new CityViewHolder(v, tvCityName, tvCountryName, tvTemperature, tvWindSpeed, imageWeather);
    }

    @Override
    public void onBindViewHolder(final CityViewHolder holder, final int position) {
        final City city = cityList.get(position);

        RESTHelper.getInstance().refreshWeather(city.getCityName(), new Callback<ModelWeather, AppError>() {
            @Override
            public void onCompleted(ModelWeather result) {
                holder.tvCityName.setText(result.getCity());
                holder.tvCountryName.setText(result.getCountry());
                holder.tvTemperature.setText(String.valueOf(result.getTemperatureC()));
                holder.tvWindSpeed.setText(String.valueOf(result.getSpeedOfWind()));
                int resID = context.getResources().getIdentifier("drawable/weather_icon_" + result.getCode(),null, context.getPackageName());
                @SuppressWarnings("deprecation")
                Drawable weatherIconDrawable = context.getResources().getDrawable(resID);
                holder.tvImageWeather.setImageDrawable(weatherIconDrawable);
            }

            @Override
            public void onError(AppError error) {
                Log.d(TAG, "onError: nothing");

            }
        });


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: pressed item " + v.getId());
                Intent intent = new Intent(context, WeatherActivity.class);
                intent.putExtra(Constants.LOCATION_NAME, city.getCityName());
                context.startActivity(intent);
            }
        });


        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                dialog.setTitle("Remove city");
                dialog.setMessage("Are you sure you want to delete this city?");
                dialog.setPositiveButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d("QQQQ", "onClick: position " + position);
                        AppFacade.getInstance().getBus().post(new DeleteItemViewEvent(position));
                    }
                });
                dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                return false;
            }
        });

    }

    @Override
    public int getItemCount() {
        if (cityList != null) {
            return cityList.size();
        }
        return 0;
    }

    class CityViewHolder extends RecyclerView.ViewHolder {
        TextView tvCityName;
        TextView tvCountryName;
        TextView tvTemperature;
        TextView tvWindSpeed;
        ImageView tvImageWeather;

        public CityViewHolder(View itemView, TextView cityName, TextView countryName,
                              TextView temperature, TextView windSpeed, ImageView imageView) {
            super(itemView);
            this.tvCityName = cityName;
            this.tvCountryName = countryName;
            this.tvTemperature = temperature;
            this.tvWindSpeed = windSpeed;
            this.tvImageWeather = imageView;
        }
    }
}
