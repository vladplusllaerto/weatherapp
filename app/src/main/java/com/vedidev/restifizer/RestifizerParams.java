package com.vedidev.restifizer;

/**
 * Created by vedi on 20/12/15.
 * Vedidev, 2015
 */
public interface RestifizerParams {
    String getClientId();
    String getClientSecret();
    String getAccessToken();
}
