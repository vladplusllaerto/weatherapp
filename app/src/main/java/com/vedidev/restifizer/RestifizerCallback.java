package com.vedidev.restifizer;

/**
 * Created by vedi on 20/12/15.
 * Vedidev, 2015
 */
public abstract class RestifizerCallback {
    public abstract void onCallback(RestifizerResponse response);
}
